package main

import (
	"context"

	"github.com/machinebox/graphql"
)

type httpRespDataStruct struct {
	Viewer Viewer `json:"viewer"`
}
type Dimensions struct {
	CacheStatus string `json:"cacheStatus"`
}
type SumEdgeResponseBytes struct {
	EdgeResponseBytes int `json:"edgeResponseBytes"`
}
type Caching struct {
	Dimensions           Dimensions           `json:"dimensions"`
	SumEdgeResponseBytes SumEdgeResponseBytes `json:"sumEdgeResponseBytes"`
}
type ResponseStatusMap struct {
	EdgeResponseStatus int `json:"edgeResponseStatus"`
	Requests           int `json:"requests"`
}
type SumResponseStatus struct {
	ResponseStatusMap []ResponseStatusMap `json:"responseStatusMap"`
}
type ResponseCodes struct {
	SumResponseStatus SumResponseStatus `json:"sumResponseStatus"`
}
type Zones struct {
	Caching       []Caching       `json:"caching"`
	ResponseCodes []ResponseCodes `json:"responseCodes"`
}
type Viewer struct {
	Zones []Zones `json:"zones"`
}

func buildHttpGraphQLQuery(startDate string, endDate string, zoneID string) *graphql.Request {

	query := graphql.NewRequest(`
	{
		viewer {
		  zones(filter: { zoneTag: $zoneTag }) {
			caching:httpRequestsAdaptiveGroups(
			  limit: 10000
			  filter: {datetimeMinute_geq: $startDate, datetimeMinute_leq: $endDate}
			) {
			  dimensions {
				cacheStatus
			  }
			  SumEdgeResponseBytes:sum {
				edgeResponseBytes
			  }
			}
		   responseCodes:httpRequests1mGroups(limit: 10000
			  filter: {datetimeMinute_geq: $startDate, datetimeMinute_leq: $endDate}) {
				SumResponseStatus:sum{
				responseStatusMap{
				  edgeResponseStatus
				  requests
				}
			  }
			}
		  }
		}
	  }
	  `)

	// set any variables
	query.Var("zoneTag", zoneID)
	query.Var("startDate", startDate)
	query.Var("endDate", endDate)

	return query
}

// Get cloudflare metrics from GraphQL using the provided api-email and api-key parameters and returns a marshalled JSON struct and an error if something went wrong during the fetch
func getCloudflareHTTPMetrics(query *graphql.Request, apiEmail string, apiKey string) (respData httpRespDataStruct, err error) {
	client := graphql.NewClient("https://api.cloudflare.com/client/v4/graphql")

	req := query

	// set header fields -> token and email!
	req.Header.Set("x-auth-key", apiKey)
	req.Header.Set("x-auth-email", apiEmail)
	// define a Context for the request
	ctx := context.Background()

	// run it and capture the response

	if err := client.Run(ctx, req, &respData); err != nil {
		return respData, err
	}
	return respData, nil
}
